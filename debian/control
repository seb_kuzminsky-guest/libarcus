Source: libarcus
Priority: optional
Maintainer: Debian 3-D Printing Packages <3dprinter-general@lists.alioth.debian.org>
Uploaders:
 Gregor Riepl <onitake@gmail.com>,
 Christoph Berg <myon@debian.org>,
Build-Depends:
 cmake,
 debhelper (>= 10.2.1),
 dh-python,
 libprotobuf-dev,
 libprotoc-dev,
 protobuf-compiler,
 python3-all-dev,
 python3-sip-dev,
 python3-sipbuild,
 sip-tools,
Standards-Version: 4.6.0
Section: libs
Homepage: https://github.com/Ultimaker/libArcus
Vcs-Browser: https://salsa.debian.org/3dprinting-team/libarcus
Vcs-Git: https://salsa.debian.org/3dprinting-team/libarcus.git

Package: libarcus3
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: message queue for Cura based on protobuf (shared library)
 This library contains C++ code and Python3 bindings for creating a socket
 in a thread and using this socket to send and receive messages based on
 the Protocol Buffers library. It is designed to facilitate the communication
 between Cura and its backend and similar code.
 This package contains the shared library.

Package: libarcus-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libarcus3 (= ${binary:Version}),
 ${misc:Depends},
Description: message queue for Cura based on protobuf (development files)
 This library contains C++ code and Python3 bindings for creating a socket
 in a thread and using this socket to send and receive messages based on
 the Protocol Buffers library. It is designed to facilitate the communication
 between Cura and its backend and similar code.
 This package contains C++ headers and other development files.

Package: python3-arcus
Section: python
Architecture: any
Multi-Arch: same
Depends:
 libarcus3 (= ${binary:Version}),
 python3-pyqt6,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
 ${sip3:Depends},
Description: message queue for Cura based on protobuf (Python bindings)
 This library contains C++ code and Python3 bindings for creating a socket
 in a thread and using this socket to send and receive messages based on
 the Protocol Buffers library. It is designed to facilitate the communication
 between Cura and its backend and similar code.
 This package contains the Python 3 bindings.
